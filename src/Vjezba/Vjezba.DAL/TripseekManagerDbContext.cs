﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;
using Project.Model;

namespace Project.DAL
{
    public class TripseekManagerDbContext : IdentityDbContext<User>
    {
        public TripseekManagerDbContext()
            : base("TripseekManagerDbContext", throwIfV1Schema: false)
        {
        }

        public static TripseekManagerDbContext Create()
        {
            return new TripseekManagerDbContext();
        }

        public DbSet<City> Cities { get; set; }

        public DbSet<Country> Countries { get; set; }

        public DbSet<Trip> Trips { get; set; }

        public DbSet<Report> Reports { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Booking> Bookings { get; set; }

        public DbSet<Transaction> Transactions { get; set; }

        public DbSet<PaymentMethod> PaymentMethods { get; set; }

        public DbSet<Favorite> Favorites { get; set; }

        // TODO Add DbSets here
    }
}
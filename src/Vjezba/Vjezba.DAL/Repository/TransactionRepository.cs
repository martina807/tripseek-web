﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Model;

namespace Project.DAL.Repository
{
    public class TransactionRepository : RepositoryBase<Transaction>
    {
        public TransactionRepository(TripseekManagerDbContext context)
            : base(context)
        {
        }

        public List<Transaction> GetList() => 
            DbContext.Transactions
                .Include(p => p.Trip)
                .Include(p => p.Trip.City)
                .Include(p => p.Trip.City.Country)
                .Include(p => p.UserCreated)
                .Include(p => p.UserCreated.City)
                .Include(p => p.UserCreated.City.Country)
                .Include(p => p.PaymentMethod)
                .ToList();
        }
}
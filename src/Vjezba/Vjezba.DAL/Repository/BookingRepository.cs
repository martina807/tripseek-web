﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Model;

namespace Project.DAL.Repository
{
    public class BookingRepository : RepositoryBase<Booking>
    {
        public BookingRepository(TripseekManagerDbContext context)
            : base(context)
        {
        }

        public List<Booking> GetList() =>
            DbContext.Bookings
                .Include(p => p.Trip)
                .Include(p => p.Trip.City)
                .Include(p => p.Trip.City.Country)
                .Include(p => p.Transaction)
                .Include(p => p.Transaction.PaymentMethod)
                .Include(p => p.UserCreated)
                .Include(p => p.UserCreated.City)
                .Include(p => p.UserCreated.City.Country)
                .ToList();

        public List<Booking> GetList(string userId) =>
            DbContext.Bookings
                .Where(p => p.CreatedById == userId)
                .Include(p => p.Transaction)
                .Include(p => p.Transaction.PaymentMethod)
                .Include(p => p.Trip)
                .Include(p => p.Trip.City)
                .Include(p => p.Trip.City.Country)
                .Include(p => p.Trip.UserCreated)
                .Include(p => p.Trip.UserCreated.City)
                .Include(p => p.Trip.UserCreated.City.Country)
                .ToList();

        public Booking Find(string userId, int tripId) =>
             DbContext.Bookings
                 .Where(p => p.CreatedById == userId && p.TripID == tripId)
                 .SingleOrDefault();

        public bool IsBooked(string userId, int bookingId) =>
            DbContext.Bookings
                .Where(p => p.CreatedById == userId && p.ID == bookingId)
                .Any();

        public override Booking Find(int id) =>
            DbContext.Bookings
                .Where(p => p.ID == id)
                .Include(p => p.Trip)
                .Include(p => p.Trip.City)
                .Include(p => p.Trip.City.Country)
                .Include(p => p.Transaction)
                .Include(p => p.Transaction.PaymentMethod)
                .Include(p => p.UserCreated)
                .Include(p => p.UserCreated.City)
                .Include(p => p.UserCreated.City.Country)
                .SingleOrDefault();
    }
}
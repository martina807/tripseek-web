﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Model;

namespace Project.DAL.Repository
{
    public class ReportRepository : RepositoryBase<Report>
    {
        public ReportRepository(TripseekManagerDbContext context)
            : base(context)
        {
        }

        public List<Trip> GetList()
        {
            return this.DbContext.Reports
                .Include(p => p.Trip)
                .Select(p => p.Trip)
                .Include(p => p.City)
                .Include(p => p.City.Country)
                .Include(p => p.UserCreated)
                .Include(p => p.UserCreated.City)
                .Include(p => p.UserCreated.City.Country)
                .ToList();
        }

        public bool isReported(string userId, int tripId)
        {
            return this.DbContext.Reports
                .Where(p => p.CreatedById == userId && p.TripID == tripId)
                .Any();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Model;

namespace Project.DAL.Repository
{
    public class CountryRepository : RepositoryBase<Country>
    {
        public CountryRepository(TripseekManagerDbContext context)
            : base(context)
        {
        }

        public List<Country> GetList() =>
            DbContext.Countries
                .OrderBy(p => p.ID)
                .ToList();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Model;

namespace Project.DAL.Repository
{
    public class PaymentMethodRepository : RepositoryBase<PaymentMethod>
    {
        public PaymentMethodRepository(TripseekManagerDbContext context)
            : base(context)
        {
        }

        public List<PaymentMethod> GetList() =>
            DbContext.PaymentMethods
                .Include(p => p.UserCreated)
                .Include(p => p.UserCreated.City)
                .Include(p => p.UserCreated.City.Country)
                .ToList();

        public PaymentMethod FindByUserId(string userId) =>
            DbContext.PaymentMethods
                .SingleOrDefault(p => p.CreatedById == userId);
    }
}
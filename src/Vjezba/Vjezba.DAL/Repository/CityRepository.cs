﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Model;

namespace Project.DAL.Repository
{
    public class CityRepository : RepositoryBase<City>
    {
        public CityRepository(TripseekManagerDbContext context)
            : base(context)
        {
        }

        public List<City> GetList() => 
            DbContext.Cities
                .OrderBy(p => p.ID)
                .ToList();
    }
}

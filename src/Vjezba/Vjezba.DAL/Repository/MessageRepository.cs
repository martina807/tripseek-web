﻿using Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.DAL.Repository
{
    public class MessageRepository : RepositoryBase<Message>
    {
        public MessageRepository(TripseekManagerDbContext context)
            : base(context)
        {
        }

        public List<Message> GetList(string userId) =>
            DbContext.Messages
                .Include(p => p.UserCreated)
                .Include(p => p.Receiver)
                .Include(p => p.Trip)
                .Include(p => p.Trip.City)
                .Include(p => p.Trip.City.Country)
                .Include(p => p.Trip.UserCreated)
                .Include(p => p.UserCreated.City)
                .Include(p => p.UserCreated.City.Country)
                .Include(p => p.Receiver.City)
                .Include(p => p.Receiver.City.Country)
                .Where(p => (p.CreatedById == userId && !p.hasSenderRemoved) || (p.ReceiverID == userId && !p.hasReceiverRemoved))
                .OrderByDescending(p => p.ID)
                .ToList();

        public override Message Find(int id) =>
            DbContext.Messages
                .Where(p => p.ID == id)
                .Include(p => p.UserCreated)
                .Include(p => p.Receiver)
                .Include(p => p.Trip)
                .Include(p => p.Trip.City)
                .Include(p => p.Trip.City.Country)
                .Include(p => p.Trip.UserCreated)
                .Include(p => p.UserCreated.City)
                .Include(p => p.UserCreated.City.Country)
                .Include(p => p.Receiver.City)
                .Include(p => p.Receiver.City.Country)
                .SingleOrDefault();

        public void Hide(int id, string userId, bool autoSave = false)
        {
            var model = this.DbContext.Set<Message>().Find(id);
            if (userId == model.CreatedById)
            {
                model.hasSenderRemoved = true;
            }
            else
            {
                model.hasReceiverRemoved = true;
            }

            this.DbContext.Entry(model).State = EntityState.Modified;

            if (autoSave)
                this.Save();
        }


        public void MarkAsRead(int id, string userId, bool autoSave = false)
        {
            var model = this.DbContext.Set<Message>().Find(id);
            if (userId != model.CreatedById)
            {
                model.hasReceiverRead = true;
            }

            this.DbContext.Entry(model).State = EntityState.Modified;

            if (autoSave)
                this.Save();
        }
    }
}
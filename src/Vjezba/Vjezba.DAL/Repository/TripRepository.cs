﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Model;

namespace Project.DAL.Repository
{
    public class TripRepository : RepositoryBase<Trip>
    {
        public TripRepository(TripseekManagerDbContext context)
            : base(context)
        {
        }

        public List<Trip> GetList(ITripFilter filter = null, string userId = null)
        {
            var query = this.DbContext.Trips
                .Include(p => p.City)
                .Include(p => p.City.Country)
                .Include(p => p.UserCreated)
                .Include(p => p.Favorites)
                .Include(p => p.Reports)
                .Include(p => p.Bookings)
            .AsQueryable();

            if (filter != null)
            {
                if (!string.IsNullOrWhiteSpace(filter?.Title))
                    query = query.Where(p => p.Title.ToLower().Contains(filter.Title.ToLower()));

                switch(filter.FilterOption)
                {
                    case FilterOption.HighestPrice:
                        query = query.OrderByDescending(p => p.Price);
                        break;
                    case FilterOption.LowestPrice:
                        query = query.OrderBy(p => p.Price);
                        break;
                    case FilterOption.Newest:
                        query = query.OrderByDescending(p => p.DateCreated);
                        break;
                    case FilterOption.Oldest:
                        query = query.OrderBy(p => p.DateCreated);
                        break;
                }
            }
            else
            {
                query = query.OrderByDescending(p => p.DateCreated);
            }

            if (!string.IsNullOrWhiteSpace(userId))
                query = query.Where(p => p.CreatedById == userId);

            return query
                .ToList();
        }

        public List<Trip> GetActiveList(ITripFilter filter = null, string userId = null)
        {
            return GetList(filter, userId)
                .Where(p => p.DepartsAt > DateTime.Now).ToList();
        }

        public override Trip Find(int id)
        {
            return this.DbContext.Trips
                .Include(p => p.City)
                .Include(p => p.City.Country)
                .Include(p => p.UserCreated)
                .Include(p => p.UserCreated.City)
                .Include(p => p.UserCreated.City.Country)
                .Where(p => p.ID == id)
                .FirstOrDefault();
        }
    }
}
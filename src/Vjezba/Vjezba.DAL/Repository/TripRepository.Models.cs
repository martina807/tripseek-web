﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.DAL.Repository
{
    public interface ITripFilter
    {
        string Title { get; }
        FilterOption FilterOption { get; }
    }

    public enum FilterOption { Newest, Oldest, LowestPrice, HighestPrice  }
}

﻿using Project.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace Project.DAL.Repository
{
    public class FavoriteRepository : RepositoryBase<Favorite>
    {
        public FavoriteRepository(TripseekManagerDbContext context)
            : base(context)
        {
        }

        public List<Favorite> GetList(string userId) =>
            DbContext.Favorites
                .Where(p => p.CreatedById == userId)
                .Include(p => p.Trip)
                .Include(p => p.Trip.City)
                .Include(p => p.Trip.City.Country)
                .Include(p => p.Trip.UserCreated)
                .Include(p => p.Trip.UserCreated.City)
                .Include(p => p.Trip.UserCreated.City.Country)
                .Include(p => p.UserCreated)
                .ToList();

        public bool IsFavorited(string userId, int tripId) =>
            DbContext.Favorites
                .Where(p => p.CreatedById == userId && p.TripID == tripId)
                .Any();

        public void DeleteForUser(string userId, int tripId, bool autoSave = false)
        {
            var entity = DbContext.Favorites
                .Where(p => p.CreatedById == userId && p.TripID == tripId)
                .FirstOrDefault();

            if(entity != null)
            {
                this.DbContext.Entry(entity).State = EntityState.Deleted;

                if (autoSave)
                    this.Save();
            }
        }
            
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Model;

namespace Project.DAL.Repository
{
    public class UserRepository 
    {
        private DbContext DbContext = null;

        public UserRepository(TripseekManagerDbContext context)
        {
            this.DbContext = context;
        }
     
        public User Find(string userId) =>
            DbContext.Set<User>()
                .Where(p => p.Id == userId)
                .FirstOrDefault();
    }
}

namespace Project.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePaymentBooking : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bookings", "FullName", c => c.String(nullable: false));
            AlterColumn("dbo.Bookings", "PhoneNumber", c => c.String(nullable: false, maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Bookings", "PhoneNumber", c => c.String());
            AlterColumn("dbo.Bookings", "FullName", c => c.String());
        }
    }
}

namespace Project.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MessageReadFlags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        TripID = c.Int(),
                        ReceiverID = c.String(maxLength: 128),
                        hasReceiverRead = c.Boolean(nullable: false),
                        hasSenderRemoved = c.Boolean(nullable: false),
                        hasReceiverRemoved = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        CreatedById = c.String(maxLength: 128),
                        UpdatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.ReceiverID)
                .ForeignKey("dbo.Trips", t => t.TripID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedById)
                .Index(t => t.TripID)
                .Index(t => t.ReceiverID)
                .Index(t => t.CreatedById)
                .Index(t => t.UpdatedById);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "UpdatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "TripID", "dbo.Trips");
            DropForeignKey("dbo.Messages", "ReceiverID", "dbo.AspNetUsers");
            DropIndex("dbo.Messages", new[] { "UpdatedById" });
            DropIndex("dbo.Messages", new[] { "CreatedById" });
            DropIndex("dbo.Messages", new[] { "ReceiverID" });
            DropIndex("dbo.Messages", new[] { "TripID" });
            DropTable("dbo.Messages");
        }
    }
}

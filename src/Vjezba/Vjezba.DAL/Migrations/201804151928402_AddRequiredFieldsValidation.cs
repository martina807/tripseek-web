namespace Project.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequiredFieldsValidation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reports", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.Messages", "Description", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Messages", "Description", c => c.String());
            AlterColumn("dbo.Reports", "Description", c => c.String());
        }
    }
}

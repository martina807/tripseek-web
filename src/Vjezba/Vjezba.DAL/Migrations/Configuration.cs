namespace Project.DAL.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using Project.Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<TripseekManagerDbContext>
    {
        private string _adminId;
        private string _userId;

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TripseekManagerDbContext context)
        {
            _userId = Guid.NewGuid().ToString();
            _adminId = Guid.NewGuid().ToString();
            seedRoles(context);
            seedUsers(context, _userId, _adminId);
            seedCities(context);
            seedCountries(context);
            seedTrips(context);
            seedPaymentMethods(context);
        }

        private void seedRoles(TripseekManagerDbContext context)
        {
            context.Roles.AddOrUpdate(p => p.Id,
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole
                {
                    Id = "1",
                    Name = "Admin"
                });
            context.Roles.AddOrUpdate(p => p.Id,
                new Microsoft.AspNet.Identity.EntityFramework.IdentityRole
                {
                    Id = "2",
                    Name = "Standard"
                });
        }

        private void seedUsers(TripseekManagerDbContext context, string userId1, string userId2)
        {
            var user1 = new User()
            {
                Id = userId1,
                PasswordHash = "AL3hWOAOvnEJS0KH0eW4ulY6VHd8CEK2lv6KdcTV024mvAIAHsQhOhB/afJUk0SP3Q==",
                SecurityStamp = "e9f553d9-f341-4ef4-9bc9-396063598f70",
                LockoutEnabled = true,
                FirstName = "Ivo",
                LastName = "Ivic",
                Email = "ivo@ivic.com",
                UserName = "ivo@ivic.com",
                CityID = 1,
                PhoneNumber = "+385929935139",
                StreetName = "Neka ulica usera",
                StreetNumber = "100"
            };

            var role1 = new IdentityUserRole();
            role1.RoleId = "2";
            role1.UserId = user1.Id;
            user1.Roles.Add(role1);

            context.Users.AddOrUpdate(user1);



            var user2 = new User()
            {
                Id = userId2,
                PasswordHash = "AL3hWOAOvnEJS0KH0eW4ulY6VHd8CEK2lv6KdcTV024mvAIAHsQhOhB/afJUk0SP3Q==",
                SecurityStamp = "e9f553d9-f341-4ef4-9bc9-396063598f70",
                LockoutEnabled = true,
                AgencyName = "Agenccio Travellio",
                Email = "agenccio@travellio.com",
                UserName = "agenccio@travellio.com",
                CityID = 2,
                PhoneNumber = "+385959991341",
                StreetName = "Neka druga ulica agencije",
                StreetNumber = "15",
            };

            var role2 = new IdentityUserRole();
            role2.RoleId = "1";
            role2.UserId = user2.Id;
            user2.Roles.Add(role2);

            context.Users.AddOrUpdate(user2);
        }

        private void seedCities(TripseekManagerDbContext context)
        {
            context.Cities.AddOrUpdate(p => p.ID,
                          new City() { ID = 1, Name = "Zagreb", PostalCode = "10000", CountryID = 1, DateCreated = DateTime.Now },
                          new City() { ID = 2, Name = "Split", PostalCode = "21000", CountryID = 1, DateCreated = DateTime.Now },
                          new City() { ID = 3, Name = "Rijeka", PostalCode = "23000", CountryID = 1, DateCreated = DateTime.Now });
        }

        private void seedCountries(TripseekManagerDbContext context)
        {
            context.Countries.AddOrUpdate(p => p.ID,
                          new Country() { ID = 1, Name = "Croatia", DateCreated = DateTime.Now });
        }

        private void seedTrips(TripseekManagerDbContext context)
        {
            context.Trips.AddOrUpdate(
                new Trip
                {
                    ID = 1,
                    Title = "Wonderful Zagreb",
                    CityID = 1,
                    CreatedById = _adminId,
                    DateCreated = DateTime.Now.AddDays(-15).AddHours(-2).AddMinutes(-25).AddSeconds(-30),
                    Description = "Visit Zagreb and enjoy!",
                    Price = 1699,
                    ImageUrl = "Images/zagreb-trip-1.jpg",
                    DepartsAt = DateTime.Now.AddDays(10).AddHours(3),
                    ReturnsAt = DateTime.Now.AddDays(15).AddHours(10),
                }
            );
        }

        private void seedPaymentMethods(TripseekManagerDbContext context)
        {
            var cardNumber = "4024007192068087";

            //var maskedNumber = "4024********8087";
            var maskedNumber = cardNumber.Substring(0, 4) + "********" + cardNumber.Substring(cardNumber.Count() - 4);

            var hash = Convert.ToBase64String(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(cardNumber)));
            //var hash = "test";

            context.PaymentMethods.AddOrUpdate(
                new PaymentMethod
                {
                    ID = 1,
                    MaskedNumber = maskedNumber,
                    Token = hash,
                    CVV = "123",
                    ExpirationDate = DateTime.Now.AddYears(2).AddMonths(3),
                    CreatedById = _userId,
                    DateCreated = DateTime.Now
                }
            );
        }
    }
}

namespace Project.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentMethodLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PaymentMethods", "MaskedNumber", c => c.String(maxLength: 16));
            AlterColumn("dbo.PaymentMethods", "CVV", c => c.String(maxLength: 3));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PaymentMethods", "CVV", c => c.String());
            AlterColumn("dbo.PaymentMethods", "MaskedNumber", c => c.String());
        }
    }
}

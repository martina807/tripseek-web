namespace Project.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBookingTransactionPayment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bookings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        PhoneNumber = c.String(),
                        TripID = c.Int(),
                        TransactionID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        CreatedById = c.String(maxLength: 128),
                        UpdatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Transactions", t => t.TransactionID, cascadeDelete: true)
                .ForeignKey("dbo.Trips", t => t.TripID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedById)
                .Index(t => t.TripID)
                .Index(t => t.TransactionID)
                .Index(t => t.CreatedById)
                .Index(t => t.UpdatedById);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TotalPrice = c.Int(nullable: false),
                        TripID = c.Int(),
                        PaymentMethodID = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        CreatedById = c.String(maxLength: 128),
                        UpdatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PaymentMethods", t => t.PaymentMethodID, cascadeDelete: true)
                .ForeignKey("dbo.Trips", t => t.TripID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedById)
                .Index(t => t.TripID)
                .Index(t => t.PaymentMethodID)
                .Index(t => t.CreatedById)
                .Index(t => t.UpdatedById);
            
            CreateTable(
                "dbo.PaymentMethods",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MaskedNumber = c.String(),
                        CVV = c.String(),
                        Token = c.String(),
                        ExpirationDate = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        CreatedById = c.String(maxLength: 128),
                        UpdatedById = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedById)
                .ForeignKey("dbo.AspNetUsers", t => t.UpdatedById)
                .Index(t => t.CreatedById)
                .Index(t => t.UpdatedById);
            
            AddColumn("dbo.AspNetUsers", "PaymentMethod_ID", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "PaymentMethod_ID");
            AddForeignKey("dbo.AspNetUsers", "PaymentMethod_ID", "dbo.PaymentMethods", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bookings", "UpdatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Bookings", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Bookings", "TripID", "dbo.Trips");
            DropForeignKey("dbo.Bookings", "TransactionID", "dbo.Transactions");
            DropForeignKey("dbo.Transactions", "UpdatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Transactions", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.Transactions", "TripID", "dbo.Trips");
            DropForeignKey("dbo.Transactions", "PaymentMethodID", "dbo.PaymentMethods");
            DropForeignKey("dbo.PaymentMethods", "UpdatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.PaymentMethods", "CreatedById", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "PaymentMethod_ID", "dbo.PaymentMethods");
            DropIndex("dbo.AspNetUsers", new[] { "PaymentMethod_ID" });
            DropIndex("dbo.PaymentMethods", new[] { "UpdatedById" });
            DropIndex("dbo.PaymentMethods", new[] { "CreatedById" });
            DropIndex("dbo.Transactions", new[] { "UpdatedById" });
            DropIndex("dbo.Transactions", new[] { "CreatedById" });
            DropIndex("dbo.Transactions", new[] { "PaymentMethodID" });
            DropIndex("dbo.Transactions", new[] { "TripID" });
            DropIndex("dbo.Bookings", new[] { "UpdatedById" });
            DropIndex("dbo.Bookings", new[] { "CreatedById" });
            DropIndex("dbo.Bookings", new[] { "TransactionID" });
            DropIndex("dbo.Bookings", new[] { "TripID" });
            DropColumn("dbo.AspNetUsers", "PaymentMethod_ID");
            DropTable("dbo.PaymentMethods");
            DropTable("dbo.Transactions");
            DropTable("dbo.Bookings");
        }
    }
}

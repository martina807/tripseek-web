namespace Project.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartsReturnsDates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Trips", "DepartsAt", c => c.DateTime(nullable: false));
            AddColumn("dbo.Trips", "ReturnsAt", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Trips", "ReturnsAt");
            DropColumn("dbo.Trips", "DepartsAt");
        }
    }
}

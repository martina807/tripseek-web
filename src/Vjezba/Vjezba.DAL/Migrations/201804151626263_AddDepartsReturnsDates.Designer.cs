// <auto-generated />
namespace Project.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddDepartsReturnsDates : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddDepartsReturnsDates));
        
        string IMigrationMetadata.Id
        {
            get { return "201804151626263_AddDepartsReturnsDates"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

﻿using Project.DAL.Repository;
using Project.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Web.Models
{
    public class TripCreateModel : Trip
    {
        public HttpPostedFileBase UploadedFile { get; set; }
    }

    public class TripFilterModel : ITripFilter
    {
        public string Title { get; set; }
        public FilterOption FilterOption { get; set; }
    }

    public class TripDetailsModel : Trip
    {
        public int? BookingID { get; set; }
        public bool IsBooked => BookingID != null;
        public PaymentMethod PaymentMethod { get; set; }
        public bool HasPaymentMethod => PaymentMethod != null;
        public User User { get; set; }
        public bool IsFavorite { get; set; }
    }

    public class TripDetailsBooking : Booking
    {
        public PaymentMethod PaymentMethod { get; set; }
        public bool HasPaymentMethod => PaymentMethod != null;

        [StringLength(16)]
        public string CardNumber { get; set; }
    }

    public class TripMyModel
    {
        public IEnumerable<Trip> Saved { get; set; }
        public IEnumerable<Trip> Booked { get; set; }
    }

}
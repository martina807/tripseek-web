﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Project.DAL.Repository;
using Ninject;

namespace Project.Web.Controllers
{
    public class BaseController : Controller
    {
        protected string userId;

        public BaseController()
        {
            userId = System.Web.HttpContext.Current.GetOwinContext().Authentication.User.Identity.GetUserId();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        protected void saveToLogFile(string action)
        {
            var fileName = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + "log\\" + "logs.txt";
            var sw = new System.IO.StreamWriter(fileName, true);
            if (userId != null)
            {
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " User: " + userId + " " + action);
            }
            else
            {
                sw.WriteLine("[" + DateTime.Now.ToString() + "]" + " " + action);
            }
            sw.Close();
        }
    }
}
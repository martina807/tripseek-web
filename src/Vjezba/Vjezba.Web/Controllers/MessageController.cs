﻿using Ninject;
using Project.DAL.Repository;
using Project.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    [Authorize]
    [RoutePrefix("Message")]
    public class MessageController : BaseController
    {
        [Inject]
        public MessageRepository MessageRepository { get; set; }

        // GET: Messages
        public ActionResult Index()
        {
            return View(MessageRepository.GetList(userId));
        }

        [HttpPost]
        [Route("Delete")]
        public JsonResult Delete(int id)
        {
            this.MessageRepository.Hide(id, userId, true);

            return new JsonResult() { Data = "OK", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [Route("{id}")]
        public ActionResult Details(int id)
        {
            this.MessageRepository.MarkAsRead(id, userId, autoSave: true);
            return View(this.MessageRepository.Find(id));
        }

        [Route("Send")]
        public ActionResult Send(Message model)
        {
            bool? success = false;
            if (ModelState.IsValid)
            {
                model.ReceiverID = model.CreatedById;
                this.MessageRepository.Add(model, userId, autoSave: true);
                saveToLogFile("Message sent: " + this.MessageRepository.GetList(userId).OrderByDescending(p => p.ID).FirstOrDefault().ID);
                success = true;
            }
            if (success.Value)
            {
                TempData["Success"] = "Message sent.";
            }
            else
            {
                TempData["Error"] = "Message failed.";
            }
            return RedirectToAction("Index");
        }
    }
}
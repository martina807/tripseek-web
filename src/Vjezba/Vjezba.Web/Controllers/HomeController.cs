﻿using Ninject;
using Project.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class HomeController : BaseController
    {
        [Inject]
        public TripRepository TripRepository { get; set; }

        public ActionResult Index()
        {
            // TODO Show only active trips where booking date is less than current date
            return this.View(this.TripRepository.GetActiveList().OrderByDescending(p => p.DateCreated).Take(3).ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}
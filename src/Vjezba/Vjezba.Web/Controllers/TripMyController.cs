﻿using Ninject;
using Project.DAL;
using Project.DAL.Repository;
using Project.Model;
using Project.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    [Authorize]
    [RoutePrefix("TripMy")]
    public class TripMyController : BaseController
    {
        [Inject]
        public CityRepository CityRepository { get; set; }

        [Inject]
        public CountryRepository CountryRepository { get; set; }

        [Inject]
        public BookingRepository BookingRepository { get; set; }

        [Inject]
        public FavoriteRepository FavoriteRepository { get; set; }

        [Inject]
        public TripRepository TripRepository { get; set; }

        private TripseekManagerDbContext context = new TripseekManagerDbContext();

        [Route("")]
        public ActionResult Index()
        {
            var favoriteList = FavoriteRepository.GetList(userId).Select(p => p.Trip);
            var bookedList = BookingRepository.GetList(userId).Select(p => p.Trip);

            return View(new TripMyModel { Saved = favoriteList, Booked = bookedList });
        }
    }
}
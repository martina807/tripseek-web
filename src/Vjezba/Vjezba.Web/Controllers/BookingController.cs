﻿using Ninject;
using Project.DAL;
using Project.DAL.Repository;
using Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class BookingController : BaseController
    {
        [Inject]
        public CityRepository CityRepository { get; set; }

        [Inject]
        public CountryRepository CountryRepository { get; set; }

        [Inject]
        public BookingRepository BookingRepository { get; set; }

        private TripseekManagerDbContext context = new TripseekManagerDbContext();

        // GET: Booking
        //[Route("{id}")]
        [Authorize]
        public ActionResult Details(int id)
        {
            Booking booking = null;

            if (BookingRepository.IsBooked(userId, id))
            {
                booking = BookingRepository.Find(id);
            }
            else
            {
                TempData["Error"] = "Error, you are not owner of this booking.";
            }
            return View(booking);
        }
    }
}
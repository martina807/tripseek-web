﻿using Ninject;
using Project.DAL.Repository;
using Project.Model;
using Project.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    //[RoutePrefix("Trip")]
    public class TripController : BaseController
    {
        [Inject]
        public TripRepository TripRepository { get; set; }

        [Inject]
        public CityRepository CityRepository { get; set; }

        [Inject]
        public CountryRepository CountryRepository { get; set; }

        [Inject]
        public ReportRepository ReportRepository { get; set; }

        [Inject]
        public FavoriteRepository FavoriteRepository { get; set; }

        [Inject]
        public MessageRepository MessageRepository { get; set; }

        [Inject]
        public BookingRepository BookingRepository { get; set; }

        [Inject]
        public PaymentMethodRepository PaymentMethodRepository { get; set; }

        [Inject]
        public TransactionRepository TransactionRepository { get; set; }

        [Inject]
        public UserRepository UserRepository { get; set; }

        // GET: Trip
        public ActionResult Index()
        {
            // TODO: Show active list only
            return this.View(this.TripRepository.GetActiveList());
        }

        //[Route("IndexAjax")]
        [HttpPost]
        public ActionResult IndexAjax(TripFilterModel model)
        {
            return PartialView("_IndexTable", this.TripRepository.GetActiveList(model));
        }

        //[Route("{id}")]
        public ActionResult Details(int id)
        {
            Trip trip = this.TripRepository.Find(id);
            bool isFavorite = false;

            PaymentMethod paymentMethod = null;
            int? bookingID = null;

            if (userId != null)
            {
                var booking = BookingRepository.Find(userId, trip.ID);
                if (booking != null)
                {
                    bookingID = booking.ID;
                }

                paymentMethod = PaymentMethodRepository.FindByUserId(userId);

                isFavorite = FavoriteRepository.IsFavorited(userId, id);
            }

            TripDetailsModel details = new TripDetailsModel
            {
                ID = trip.ID,
                Title = trip.Title,
                Description = trip.Description,
                Price = trip.Price,
                CityID = trip.CityID,
                ImageUrl = trip.ImageUrl,
                CreatedById = trip.CreatedById,
                UserCreated = trip.UserCreated,
                City = trip.City,
                BookingID = bookingID,
                DepartsAt = trip.DepartsAt,
                ReturnsAt = trip.ReturnsAt,
                PaymentMethod = paymentMethod,
                User = UserRepository.Find(userId),
                IsFavorite = isFavorite
            };

            return View(details);
        }

        [Authorize]
        [HttpPost]
        //[Route("Contact")]
        public ActionResult Contact(Message model)
        {
            if (ModelState.IsValid)
            {
                this.MessageRepository.Add(model, userId, autoSave: true);
                saveToLogFile("Message sent: " + this.MessageRepository.GetList(userId).OrderByDescending(p => p.ID).FirstOrDefault().ID);
                TempData["Success"] = "Message sent.";
            }
            else
            {
                TempData["Error"] = "Message failed.";
            }
            var trip = TripRepository.Find(model.TripID.Value);
            return RedirectToAction("Details", new { id = model.TripID.Value });
        }

        [Authorize]
        [HttpPost]
        //[Route("Report")]
        public ActionResult Report(Report model)
        {
            if (ModelState.IsValid)
            {
                if (this.ReportRepository.isReported(userId, model.TripID.Value))
                {
                    TempData["Error"] = "Already reported.";
                }
                else
                {
                    this.ReportRepository.Add(model, userId, autoSave: true);
                    saveToLogFile("Reported: " + this.ReportRepository.GetList().OrderByDescending(p => p.ID).FirstOrDefault().ID);

                    TempData["Success"] = "Report sent.";
                }
            }
            else
            {
                TempData["Error"] = "Report failed.";
            }

            var trip = TripRepository.Find(model.TripID.Value);
            return RedirectToAction("Details", new { id = model.TripID.Value });
        }

        [Authorize]
        [HttpPost]
        //[Route("Booking")]
        public ActionResult Booking(TripDetailsBooking model)
        {
            if (this.BookingRepository.IsBooked(userId, model.TripID.Value))
            {
                TempData["Error"] = "Already booked.";
            }
            else
            {
                PaymentMethod paymentMethod = null;

                if (model.HasPaymentMethod)
                {
                    paymentMethod = PaymentMethodRepository.FindByUserId(userId);
                    if (paymentMethod == null)
                    {
                        var maskedNumber = model.CardNumber.Substring(0, 4) + "********" + model.CardNumber.Substring(model.CardNumber.Count() - 4);
                        var hash = Convert.ToBase64String(MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(model.CardNumber)));

                        model.CardNumber = null;

                        model.PaymentMethod.MaskedNumber = maskedNumber;
                        model.PaymentMethod.Token = hash;

                        PaymentMethodRepository.Add(model.PaymentMethod, userId, true);
                        paymentMethod = PaymentMethodRepository.FindByUserId(userId);
                    }
                }

                if (paymentMethod != null)
                {
                    var transaction = new Transaction
                    {
                        PaymentMethodID = paymentMethod.ID,
                        TripID = model.TripID,
                        CreatedById = userId,
                        TotalPrice = TripRepository.Find(model.TripID.Value).Price.Value
                    };

                    this.TransactionRepository.Add(transaction, userId, true);

                    model.TransactionID = this.TransactionRepository.GetList().OrderByDescending(p => p.ID).FirstOrDefault().ID;

                    var booking = new Booking
                    {
                        PhoneNumber = model.PhoneNumber,
                        FullName = model.FullName,
                        TransactionID = model.TransactionID,
                        TripID = model.TripID
                    };

                    this.BookingRepository.Add(booking, userId, autoSave: true);
                    var bookingId = this.BookingRepository.GetList().OrderByDescending(p => p.ID).FirstOrDefault().ID;
                    saveToLogFile("Booked: " + bookingId);

                    TempData["Success"] = "Booking completed";

                    return RedirectToAction("Details", "Booking", new { id = bookingId });
                }
                else
                {
                    TempData["Error"] = "Booking failed, no payment method for this user.";
                }
            }

            return RedirectToAction("Details", new { id = BookingRepository.Find(model.TripID.Value) });
        }

        [Authorize]
        //[Route("Create")]
        [HttpGet]
        public ActionResult Create()
        {
            this.FillDropDownValues();
            return View();
        }

        [Authorize]
        //[Route("Create")]
        [HttpPost]
        public ActionResult Create(Trip model, HttpPostedFileBase UploadedFile)
        {
            string guid = Guid.NewGuid().ToString();

            string path = Server.MapPath("~/Images/" + guid + "_" + UploadedFile.FileName);
            string relativePath = "Images/" + guid + "_" + UploadedFile.FileName;
            UploadedFile.SaveAs(path);

            model.ImageUrl = relativePath;
            model.City = CityRepository.Find(model.CityID.Value);
            model.City.Country = CountryRepository.Find(model.City.CountryID.Value);

            this.TripRepository.Add(model, userId, autoSave: true);

            saveToLogFile("Trip created: " + this.TripRepository.GetList().OrderByDescending(p => p.ID).FirstOrDefault().ID);

            return RedirectToAction("Index");
        }

        [HttpGet]
        //[ActionName("Edit")]
        public ActionResult Edit(int id)
        {
            this.FillDropDownValues();

            var model = this.TripRepository.Find(id);

            return View(model);
        }

        [HttpPost]
        //[ActionName("Edit")]
        public ActionResult EditPost(int id, HttpPostedFileBase UploadedFile)
        {
            var model = this.TripRepository.Find(id);

            if (UploadedFile != null)
            {
                string guid = Guid.NewGuid().ToString();

                string path = Server.MapPath("~/Images/" + guid + "_" + UploadedFile.FileName);
                string relativePath = "Images/" + guid + "_" + UploadedFile.FileName;
                UploadedFile.SaveAs(path);
                model.ImageUrl = relativePath;
            }

            var didUpdateModelSucceed = this.TryUpdateModel(model);

            if (didUpdateModelSucceed && ModelState.IsValid)
            {
                this.TripRepository.Update(model, autoSave: true);
                saveToLogFile("Trip edited: " + id);
                return RedirectToAction("Details", new { id = model.ID });
            }

            this.FillDropDownValues();
            return View(model);
        }

        [Authorize]
        //[Route("Favorite")]
        [HttpPost]
        public JsonResult Favorite(int id)
        {
            var model = new Favorite { TripID = id };

            string status = "";

            if (this.FavoriteRepository.IsFavorited(userId, id))
            {
                this.FavoriteRepository.DeleteForUser(userId, id, true);
                status = "Add to favorites";
            }
            else
            {
                status = "Remove from favorites";
                this.FavoriteRepository.Add(model, userId, autoSave: true);
                saveToLogFile("Favorited: " + this.FavoriteRepository.GetList(userId).OrderByDescending(p => p.ID).FirstOrDefault().ID);
            }

            return new JsonResult() { Data = status, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private void FillDropDownValues()
        {
            FillDropDownCities();
            FillDropDownCountries();
        }

        private void FillDropDownCities()
        {
            var possibleCities = this.CityRepository.GetList();

            var selectItems = new List<SelectListItem>();

            //Polje je opcionalno
            var listItem = new SelectListItem();
            listItem.Text = "- choose -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var city in possibleCities)
            {
                listItem = new SelectListItem();
                listItem.Text = city.Name;
                listItem.Value = city.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }

            ViewBag.PossibleCities = selectItems;
        }

        private void FillDropDownCountries()
        {
            var possibleCountries = this.CountryRepository.GetList();

            var selectItems = new List<SelectListItem>();

            //Polje je opcionalno
            var listItem = new SelectListItem();
            listItem.Text = "- choose -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var country in possibleCountries)
            {
                listItem = new SelectListItem();
                listItem.Text = country.Name;
                listItem.Value = country.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }

            ViewBag.PossibleCountries = selectItems;
        }
    }
}
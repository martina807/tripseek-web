﻿using Ninject;
using Project.DAL;
using Project.DAL.Repository;
using Project.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Controllers
{
    public class UserController : BaseController
    {
        [Inject]
        public CityRepository CityRepository { get; set; }

        [Inject]
        public CountryRepository CountryRepository { get; set; }


        // GET: User
        public ActionResult Index(string id)
        {
            TripseekManagerDbContext context = new TripseekManagerDbContext();
            var model = GetList(context).Where(p => p.Id == id).FirstOrDefault();
            return View(model);
        }

        // GET: User
        [HttpGet]
        public ActionResult Edit(string id)
        {
            TripseekManagerDbContext context = new TripseekManagerDbContext();
            this.FillDropDownValues();
            var model = GetList(context).Where(p => p.Id == id).FirstOrDefault();
            return View(model);
        }

        // POST: User
        [HttpPost]
        public ActionResult EditPost(string id)
        {
            TripseekManagerDbContext context = new TripseekManagerDbContext();
            var model = GetList(context).Where(p => p.Id == id).FirstOrDefault();

            var didUpdateModelSucceed = this.TryUpdateModel(model);

            if (didUpdateModelSucceed && ModelState.IsValid)
            {
                context.Entry(model).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index", new { id = id });
            }

            this.FillDropDownValues();
            return View(model);
        }

        public List<User> GetList(TripseekManagerDbContext context)
        {
            return context.Users
                .Include(p => p.City)
                .Include(p => p.City.Country)
                .ToList();
        }


        private void FillDropDownValues()
        {
            FillDropDownCities();
            FillDropDownCountries();
        }

        private void FillDropDownCities()
        {
            var possibleCities = this.CityRepository.GetList();

            var selectItems = new List<SelectListItem>();

            var listItem = new SelectListItem();
            listItem.Text = "- choose -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var city in possibleCities)
            {
                listItem = new SelectListItem();
                listItem.Text = city.Name;
                listItem.Value = city.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }

            ViewBag.PossibleCities = selectItems;
        }

        private void FillDropDownCountries()
        {
            var possibleCountries = this.CountryRepository.GetList();

            var selectItems = new List<SelectListItem>();

            var listItem = new SelectListItem();
            listItem.Text = "- choose -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var country in possibleCountries)
            {
                listItem = new SelectListItem();
                listItem.Text = country.Name;
                listItem.Value = country.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }

            ViewBag.PossibleCountries = selectItems;
        }
    }
}
﻿Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

$(document).ready(function () {

    var priceEur = $(".price-eur");

    if (priceEur) {
        $.getJSON("http://api.fixer.io/latest?symbols=EUR,HRK", function (data) {
            $(".price-eur").each(function () {
                var price = $(this).html().replace(/\,/g, '') * data.rates.HRK;
                console.log(price);
                console.log($(this).closest(".price-hrk"));
                $(this).closest("div").find(".price-hrk").html(price.formatMoney(0));
            });
            //var price = data.rates.HRK * priceEur.html().replace(/\,/g, '');

        });
    }



});
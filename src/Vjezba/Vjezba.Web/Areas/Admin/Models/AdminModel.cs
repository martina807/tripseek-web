﻿using Project.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Web.Areas.Admin.Models
{
    public class AdminModel
    {
        public List<TripAdmin> AllUnique { get; set; }
        public List<TripAdmin> Active { get; set; }
        public List<TripAdmin> Inactive { get; set; }
        public List<TripAdmin> Sold { get; set; }
        public List<TripAdmin> Favorited { get; set; }
        public List<TripAdmin> Reported { get; set; }
    }

    public class TripAdmin : Trip
    {
        public int Amount { get; set; }
    }
}
﻿using Ninject;
using Project.DAL.Repository;
using Project.Model;
using Project.Web.Areas.Admin.Models;
using Project.Web.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Project.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    [RouteArea("Admin", AreaPrefix = "Admin")]
    [RoutePrefix("")]
    public class TripAdminController : BaseController
    {
        [Inject]
        public TripRepository TripRepository { get; set; }

        [Inject]
        public ReportRepository ReportRepository { get; set; }

        [Inject]
        public FavoriteRepository FavoriteRepository { get; set; }

        // GET: Admin/Trip
        [Route("")]
        public ActionResult Index()
        {
            var trips = this.TripRepository.GetList(null, userId);

            var allUnique = MapToAdminItems(trips.OrderByDescending(p => p.ID));
            var active = MapToAdminItems(trips.Where(p => p.DepartsAt > DateTime.Now).OrderByDescending(p => p.ID));
            var inactive = MapToAdminItems(trips.Where(p => p.DepartsAt <= DateTime.Now).OrderByDescending(p => p.ID));
            var sold = MapToSoldItems(trips.OrderByDescending(p => p.Bookings.Count()));
            var favorite = MapToFavoritedItems(trips.OrderByDescending(p => p.Favorites.Count()));
            var reported = MapToReportedItems(trips.OrderByDescending(p => p.Reports.Count()));

            AdminModel model = new AdminModel
            {
                AllUnique = allUnique.ToList(),
                Active = active.ToList(),
                Inactive = inactive.ToList(),
                Sold = sold.ToList(),
                Favorited = favorite.ToList(),
                Reported = reported.ToList()
            };

            return this.View(model);
        }

        private List<TripAdmin> MapToAdminItems(IEnumerable<Trip> trips)
        {
            List<TripAdmin> items = new List<TripAdmin>();

            trips.ToList().ForEach(p =>
            {
                items.Add(
                    new TripAdmin
                    {
                        ID = p.ID,
                        Title = p.Title,
                        City = p.City,
                        Price = p.Price,
                        DateCreated = p.DateCreated
                    }
               );
            });

            return items;
        }


        private List<TripAdmin> MapToSoldItems(IEnumerable<Trip> trips)
        {
            List<TripAdmin> items = new List<TripAdmin>();

            trips.ToList().ForEach(p =>
            {
                items.Add(
                    new TripAdmin
                    {
                        ID = p.ID,
                        Title = p.Title,
                        Amount = p.Bookings.Count,
                        City = p.City,
                        Price = p.Price,
                        DateCreated = p.DateCreated
                    }
               );
            });

            return items;
        }

        private List<TripAdmin> MapToReportedItems(IEnumerable<Trip> trips)
        {
            List<TripAdmin> items = new List<TripAdmin>();

            trips.ToList().ForEach(p =>
            {
                items.Add(
                    new TripAdmin
                    {
                        ID = p.ID,
                        Title = p.Title,
                        Amount = p.Reports.Count,
                        City = p.City,
                        Price = p.Price,
                        DateCreated = p.DateCreated
                    }
               );
            });

            return items;
        }


        private List<TripAdmin> MapToFavoritedItems(IEnumerable<Trip> trips)
        {
            List<TripAdmin> items = new List<TripAdmin>();

            trips.ToList().ForEach(p =>
            {
                items.Add(
                    new TripAdmin
                    {
                        ID = p.ID,
                        Title = p.Title,
                        Amount = p.Favorites.Count,
                        City = p.City,
                        Price = p.Price,
                        DateCreated = p.DateCreated
                    }
               );
            });

            return items;
        }
    }
}
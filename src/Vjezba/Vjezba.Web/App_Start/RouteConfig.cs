﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Project.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Trip_Details",
                url: "Trip/{id}",
                defaults: new { controller = "Trip", action = "Details" },
                constraints: new { id = "[0-9]+" }
            );

            routes.MapRoute(
                name: "Booking_Details",
                url: "Booking/{id}",
                defaults: new { controller = "Booking", action = "Details" },
                constraints: new { id = "[0-9]+" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

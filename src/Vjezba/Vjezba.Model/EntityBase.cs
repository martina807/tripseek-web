﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public abstract class EntityBase
    {
        [Key]
        public int ID { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [ForeignKey("UserCreated")]
        public string CreatedById { get; set; }

        public User UserCreated { get; set; }

        [ForeignKey("UserUpdated")]
        public string UpdatedById { get; set; }

        public User UserUpdated { get; set; }
    }
}

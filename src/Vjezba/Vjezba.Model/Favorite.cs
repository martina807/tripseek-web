﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Favorite : EntityBase
    {
        [ForeignKey("Trip")]
        public int? TripID { get; set; }

        public Trip Trip { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Project.Model
{
    public class Message : EntityBase
    {
        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [ForeignKey("Trip")]
        public int? TripID { get; set; }

        [ForeignKey("Receiver")]
        public string ReceiverID { get; set; }

        public User Receiver { get; set; }

        public Trip Trip { get; set; }

        public bool hasReceiverRead { get; set; } = false;

        public bool hasSenderRemoved { get; set; } = false;

        public bool hasReceiverRemoved { get; set; } = false;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Trip : EntityBase
    {
        public Trip()
        {
        }

        [Required]
        [StringLength(100, MinimumLength = 5)]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DataType(DataType.Currency)]
        [Display(Prompt ="Price in EUR")]
        [Required]
        public int? Price { get; set; }

        [DisplayName("City")]
        [ForeignKey("City")]
        public int? CityID { get; set; }

        public City City { get; set; }

        public virtual ICollection<Favorite> Favorites { get; set; }

        public virtual ICollection<Report> Reports { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }

        public string ImageUrl { get; set; }

        [DisplayName("Departs at")]
        [DataType(DataType.Date)]
        public DateTime DepartsAt { get; set; }

        [DisplayName("Returns at")]
        [DataType(DataType.Date)]
        public DateTime ReturnsAt { get; set; }
    }
}
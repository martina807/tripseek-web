﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Project.Model
{
    public class User : IdentityUser
    {
        [DisplayName("First name")]
        public string FirstName { get; set; }

        [DisplayName("Last name")]
        public string LastName { get; set; }

        [DisplayName("Street name")]
        public string StreetName { get; set; }

        [DisplayName("Street number")]
        public string StreetNumber { get; set; }

        [DisplayName("Agency name")]
        public string AgencyName { get; set; }

        [DisplayName("City")]
        [ForeignKey("City")]
        public int? CityID { get; set; }

        public City City { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        // TODO List of trips
        public virtual ICollection<Trip> Trips { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}

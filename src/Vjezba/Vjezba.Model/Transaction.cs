﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Transaction : EntityBase
    {
        [DataType(DataType.Currency)]
        public int TotalPrice { get; set; }

        [ForeignKey("Trip")]
        public int? TripID { get; set; }

        public Trip Trip { get; set; }

        [ForeignKey("PaymentMethod")]
        public int PaymentMethodID { get; set; }

        public PaymentMethod PaymentMethod { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public class PaymentMethod : EntityBase
    {
        [DisplayName("Credit card number")]
        [MinLength(16)]
        [MaxLength(16)]
        public string MaskedNumber { get; set; }

        [MinLength(3)]
        [MaxLength(3)]
        public string CVV { get; set; }

        public string Token { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ExpirationDate { get; set; }
    }
}
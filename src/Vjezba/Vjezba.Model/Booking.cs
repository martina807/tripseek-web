﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Booking : EntityBase
    {
        [DisplayName("Passenger full name")]
        [Required(AllowEmptyStrings = false)]
        public string FullName { get; set; }

        [DisplayName("Passenger phone number")]
        [Required(AllowEmptyStrings = false)]
        [MinLength(6)]
        [MaxLength(30)]
        public string PhoneNumber { get; set; }

        [ForeignKey("Trip")]
        public int? TripID { get; set; }

        public Trip Trip { get; set; }

        [ForeignKey("Transaction")]
        public int TransactionID { get; set; }

        public Transaction Transaction { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Model
{
    public class Country : EntityBase
    {
        public string Name { get; set; }

        public virtual ICollection<City> Cities { get; set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Project.Model
{
    public class City : EntityBase
    {
        public string PostalCode { get; set; }

        public string Name { get; set; }

        [DisplayName("Country")]
        [ForeignKey("Country")]
        public int? CountryID { get; set; }

        //[Required]
        public Country Country { get; set; }

        public virtual ICollection<Trip> Trips { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }

}
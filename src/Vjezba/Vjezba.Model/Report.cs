﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Report : EntityBase
    {
        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [ForeignKey("Trip")]
        public int? TripID { get; set; }

        public Trip Trip { get; set; }
    }
}
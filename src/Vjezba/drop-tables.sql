﻿DROP TABLE messages;
DROP TABLE reports;
drop table favorites;
DROP TABLE bookings;
DROP TABLE transactions;

DROP TABLE paymentmethods;

DROP TABLE __MigrationHistory;

drop table trips;

DROP TABLE AspNetUserClaims;
DROP TABLE aspnetroles;
drop table AspNetUsers;
drop table AspNetUserRoles;
drop table AspNetUserLogins;

DROP TABLE cities;
DROP TABLE countries;


